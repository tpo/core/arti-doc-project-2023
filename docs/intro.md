---
slug: /
title: Getting started with Arti
id: getting-started
---

# Introduction
---

[Arti](https://gitlab.torproject.org/tpo/core/arti) is a complete rewrite of the [C Tor](https://gitweb.torproject.org/tor.git/) codebase, and it is currently under active development. It is written in Rust, and it is designed to be modular, reusable, and easy to audit. 

:::info
Arti is not yet ready for production use, but it is ready for testing and experimentation.
:::

### Getting started
- [Compiling Arti](/guides/compiling-arti)
- [Starting Arti as a SOCKS proxy](/guides/starting-arti)
- [Configuring applications to use Arti](/guides/configuring-arti)
- [Connecting to Onion Services](/guides/connecting-to-onion)
- [CLI Reference](/guides/cli-reference)

### Migrating from Tor?
- [Using Arti with Tor Browser](/integrating-arti/using-tor)
- [Protocol support and compatibility](/guides/compatibility)
- [Capability and limitations](/guides/capability-limitations)

### On censorship with Arti
- [Using Arti with bridges](/censorship/bridges)
- [Pluggable transports](/censorship/pluggable-transports)

### Contribute to Arti
- [How to contribute](/contributing/)
- [Code of conduct](/contributing/code-of-conduct)
- [Support policy](/contributing/support-policy)
